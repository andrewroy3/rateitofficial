<?php
	session_start();
	include_once 'dbh.php';
	date_default_timezone_set('America/Los_Angeles');	
?>

<!DOCTYPE HTML>
<!--
	Halcyonic by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>One Column - Halcyonic by HTML5 UP</title>
        <link rel="shortcut icon" type="image/jpg" href="uploads/tv2.jpg">
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"
		  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
		  crossorigin="anonymous"></script>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
        
		
	</head>
	<body class="subpage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">
						<div class="row">
							<div class="12u">

								<!-- Logo -->
									<h1><a href="Profile.php?type=0" id="logo">RateIt</a></h1>

								<!-- Nav -->
									<nav id="nav">
                                        <?php
										$a = "0";
									//	echo '<a href="index.php">Homepage</a>';
									//	echo '<a href="MyRatings.php">My Ratings</a>';
									//	echo '<a href="Explore.php">Explore</a>';
									//	echo '<a href="Community.php">Community</a>';
										echo '<a href="Profile.php?type='.$a.'">Profile</a>';
										//'<a href="includes/delete.inc.php?piece=' . $row['piece'] . '&type='.$type.'">Delete</a>'
										?>
                                        
									</nav>
                                    <div class="nav-login">
                                    <?php
										if (isset($_SESSION['u_id'])){
										echo '<form class="log" action="includes/logout.inc.php" method="POST">
                                    		<button type="submit" name="submit">Logout</button>
                                   			 </form>';	
										} else{
											echo '<form class="log" action="includes/login.inc.php" method="POST">
                                        	<input type="text" name="uid" placeholder="Username/email">
                                            <input type="password" name="pwd" placeholder="password">
                                            <button type="submit" name="submit">Login</button>
                                         </form>
										 
                                         <a href="signup.php">Sign up</a>';	
										}
									?>
                                       
                                     </div> 

							</div>
						</div>
					</header>
				</div>

			<!-- Content -->
				<div id="content-wrapper">
					<div id="content">
						<div class="container">
							<div class="row">
								<div class="12u">

									<!-- Main Content -->
										<section>
											<header class="ProfHead">
                                            
												<h2>
                                                
                                                <?php
												if (isset($_SESSION['u_id'])){
													$id= $_SESSION['u_id'];
    												$sql = "SELECT * FROM users WHERE user_id='$id'";
													$result = mysqli_query($conn, $sql);
													$row = mysqli_fetch_assoc($result);
													
													echo $row ['user_uid'];
												}
												?>
                                                </h2>
                                                
                                                <?php
												//put this in to get only one user pic per page
												if (isset($_SESSION['u_id'])){
													$id= $_SESSION['u_id'];
													$sql = "SELECT * FROM users WHERE user_id='$id'";
													$result = mysqli_query($conn, $sql);
													if (mysqli_num_rows($result) > 0){
														while ($row = mysqli_fetch_assoc($result)){
															$id = $row['user_id'];
															$sqlImg = "SELECT * FROM profileimg WHERE userid= '$id'";
															$resultImg = mysqli_query($conn, $sqlImg);
															while($rowImg = mysqli_fetch_assoc($resultImg)){
																echo "<div class='user-container'>";
																	if ($rowImg['status'] == 0){
																		echo "<img src='uploads/profile".$id.".jpg'>";		
																	} else{
																		echo "<img src='uploads/profiledefault.jpg'>";	
																	}
																	//for printing username below
																	//echo "<p>".$row['user_uid']."</p>";
																echo "</div>";
															}
														}
													} else{
														echo "There are no users yet";	
													}
												}
												
                                               	if (isset($_SESSION['u_id'])){
													$a = "0";

													echo"<form class='adding' action='uploads.php' method='POST' enctype='multipart/form-data'>
													<input type='file' name='file' >
													<button type='submit' name='submit'>UPLOAD</button>
													</form>";
													echo '<a href="EnterFriend.php?type='.$a.'">Friends</a>';
													echo'</br>';
																										
												}
												
												$all = "0";
												$movie = "1";
												$tv = "2";
												$book = "3";
												$game = "4";
												$music = "5";
												echo ' <a href="Profile.php?type='.$all.'">All</a>';
                                                echo ' <a href="Profile.php?type='.$movie.'">Movies</a>';
                                                echo ' <a href="Profile.php?type='.$tv.'">TV</a>';
                                                echo ' <a href="Profile.php?type='.$book.'">Books</a>';
                                                echo ' <a href="Profile.php?type='.$game.'">Games</a>';
                                                echo ' <a href="Profile.php?type='.$music.'">Music</a>';

													//echo'<a href="Profile.php?type='.$movie.'">Movies</a>';
												?>
                                                
                                                
                                                <div id ="edit">
                                                <?php
												if (isset($_SESSION['u_id'])){
													//mysqli num rows for loop?
													$sql = "SELECT * FROM pieces3";
													
													
													$id= $_SESSION['u_id'];
    												//$sql = "SELECT * FROM pieces3 WHERE user_id='$id'";
													
													//get ratings into array
													//print if matches rating
													//for loop outside while loop for each rating in array
													//make array of pieces
													//if piece not in array move on
													//delete piece from array when found
													//also end loop once one printed
													 
													
														$result = mysqli_query($conn, $sql);
														$rArray = array();
														$pArray = array();
														$dArray = array();
														 
														while($row = $result->fetch_assoc()){
															if($row['id'] == $id){	
															
															array_push($dArray, $row['date'] );
															
															}
													 	}
														rsort($dArray);
														//sorted array r and p
														
														//make array of dates and sort, then connect to piece and rest
														
														$type = $_GET['type'];

															foreach ($dArray as &$value){
																$result = mysqli_query($conn, $sql);
																$i = 0;
																 while($row = $result->fetch_assoc() ){
																	 $piecePLC = $row['piece'];
																	 if(($row['id'] == $id) && ($row['date'] == $value) && ($i < 1) ){	
																	 	echo "<table class='RateTable'>";
																		echo'<tr class"MasterRow">';
																		if($row['type'] == 1){
																		 echo '<td class="typeRow">',"<img src='uploads/movie2.JPG'>",'</td>';
																		}
																		if($row['type'] == 2){
																		 echo '<td class="typeRow">',"<img src='uploads/tv2.JPG'>",'</td>';
																		}
																		if($row['type'] == 3){
																		 echo '<td class="typeRow">',"<img src='uploads/book5.JPG'>",'</td>';
																		}
																		if($row['type'] == 4){
																		 echo '<td class="typeRow">',"<img src='uploads/game2.JPG'>",'</td>';
																		}
																		if($row['type'] == 5){
																		 echo '<td class="typeRow">',"<img src='uploads/music2.JPG'>",'</td>';
																		}
																		  echo '<td class="PieceRow">',$piecePLC,'</td>';
																		  echo '<td class="RatingRow">';
																		  //if statement for value that changes color
																		  if($row['rating'] > 75){
																		  echo "<div style='border:1px solid black;padding:3px;color:white;background-color:green;display:inline;'>".$row['rating']. "</div>";
																		  }
																		  if(($row['rating'] > 59) && (($row['rating'] <= 75)) ){
																		  echo "<div style='border:1px solid black;padding:3px;background-color:lime;display:inline;'>".$row['rating']. "</div>";
																		  }
																		  if(($row['rating'] > 39) && (($row['rating'] <= 59)) ){
																		  echo "<div style='border:1px solid black;padding:3px;background-color:yellow;display:inline;'>".$row['rating']. "</div>";
																		  }
																		  if($row['rating'] <= 39){
																		  echo "<div style='border:1px solid black;padding:3px;color:white;background-color:red;display:inline;'>".$row['rating']. "</div>";
																		  }
																		  echo'</td>';
																		  
																		  $date= date("M d, Y", $row['date']);					  
																		  echo '<td class="dateRow">';
																		  echo "".$date.""; 
																		  echo'</td>';
																		  
																		  echo "\n";
																		  echo'<td class="ButtonRow">';
																		  echo '<a href="includes/delete.inc.php?piece=' . $row['piece'] . '&type='.$type.'">Delete</a>'
																		  . '<form class="adding" action="includes/edit.inc.php?piece=' . $row['piece'] . '&type='.$type.'" method="POST" style="display:inline;">
																			<input type="text" name="rating" placeholder="Edit" style="width:60px;height:32px; ">
																			<button type="submit" name="submit">ReRate</button>														
																			</form>';
																		  echo '</td>';
																		  echo '</tr>';
																		  echo "</table>";		
																		  //delete piece from array here
																																	  
																		  $i = 1;
																												  
																	 }  
																 }
															}
														
														 
																										
												}
												?>
                                                </div>
                                                

                                                
                                                
											</header>
											<nav>
                                            	<ul>
                                                
                                                <li><a href = "signup.php">Sign up</a></li>
                                                </ul>
                                            </nav>
	
										</section>

								</div>
							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<footer id="footer" class="container">
						<div class="row">
							<div class="8u 12u(mobile)">

								<!-- Links -->
									<section>
										<h2>Links to Important Stuff</h2>
										<div>
											<div class="row">
												<div class="3u 12u(mobile)">
													<ul class="link-list last-child">
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
												</div>
												<div class="3u 12u(mobile)">
													<ul class="link-list last-child">
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
												</div>
												<div class="3u 12u(mobile)">
													<ul class="link-list last-child">
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
												</div>
												<div class="3u 12u(mobile)">
													<ul class="link-list last-child">
														<li><a href="#">Neque amet dapibus</a></li>
														<li><a href="#">Sed mattis quis rutrum</a></li>
														<li><a href="#">Accumsan suspendisse</a></li>
														<li><a href="#">Eu varius vitae magna</a></li>
													</ul>
												</div>
											</div>
										</div>
									</section>

							</div>
							<div class="4u 12u(mobile)">

								<!-- Blurb -->
									<section>
										<h2>An Informative Text Blurb</h2>
										<p>
											Duis neque nisi, dapibus sed mattis quis, rutrum accumsan sed. Suspendisse eu
											varius nibh. Suspendisse vitae magna eget odio amet mollis. Duis neque nisi,
											dapibus sed mattis quis, sed rutrum accumsan sed. Suspendisse eu varius nibh
											lorem ipsum amet dolor sit amet lorem ipsum consequat gravida justo mollis.
										</p>
									</section>

							</div>
						</div>
					</footer>
				</div>

			<!-- Copyright -->
				<div id="copyright">
					&copy; Untitled. All rights reserved. | Design: <a href="http://html5up.net">HTML5 UP</a>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
            
            <script>
            $(document).ready(function(){
				$("#menu-toggle").click(function(){
					$("#edit").toggle();
				});
			});
            </script>

	</body>
</html>