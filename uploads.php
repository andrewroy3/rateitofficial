<?php
session_start();
include_once 'dbh.php';
$id = $_SESSION['u_id'];

if(isset($_POST['submit'])){
	$file = $_FILES['file'];
	
	$fileName = $_FILES['file']['name'];
	$fileTmpName = $_FILES['file']['tmp_name'];
	$fileSize = $_FILES['file']['size'];
	$fileError = $_FILES['file']['error'];
	$fileType = $_FILES['file']['type'];
	
	$fileExt = explode('.', $fileName);
	$fileActualExt = strtolower(end($fileExt));
	
	$allowed = array('jpg');
	
	if(in_array($fileActualExt, $allowed)){
		if($fileError === 0){
			if($fileSize < 1000000){
				$fileNameNew = "profile".$id.".".$fileActualExt;
				$fileDestination = 'uploads/'.$fileNameNew;
				move_uploaded_file($fileTmpName, $fileDestination);
				//issue=user id cant equal id because no id in there
				$sql = "UPDATE profileimg SET status=0 WHERE userid='$id';";
				$result = mysqli_query($conn, $sql);
				header("Location: Profile.php?type=uploadsuccess");	
			} else{
				header("Location: Profile.php?type=uploadwrongsize");	
			}
		} else{
			header("Location: Profile.php?type=uploadpicerror");
		}
	}else{
		header("Location: Profile.php?type=uploadwrongtype");	
	}
}